#ifndef Z0_TEST_H
#define Z0_TEST_H

#include <stdio.h>

#define Z0_TEST_TERM_GRN "\x1B[32m"
#define Z0_TEST_TERM_RED "\x1B[31m"
#define Z0_TEST_TERM_DEF "\x1B[0m"

#define Z0_TEST_NAME(name) printf("========== %s ==========\n", name)
#define Z0_TEST_DESC(text) printf("DESC:   %s\n", text)
#define Z0_TEST_ASSERT(condition) \
	if ((condition)) { \
		printf("RESULT: " Z0_TEST_TERM_GRN "PASSED" Z0_TEST_TERM_DEF "\n"); \
	} else { \
		printf("RESULT: " Z0_TEST_TERM_RED "FAILED" Z0_TEST_TERM_DEF "\n"); \
	}

#define Z0_CONCAT(a, b) a ## b
#define Z0_MACRO_VAR(name) Z0_CONCAT(name, __LINE__)
#define Z0_TEST_BENCHMARK(cnt, iter) \
	for(clock_t Z0_MACRO_VAR(__i__) = 0, Z0_MACRO_VAR(__start__) = clock(), Z0_MACRO_VAR(__end__); \
		Z0_MACRO_VAR(__i__) < cnt; \
		(Z0_MACRO_VAR(__i__) += 1), (Z0_MACRO_VAR(__end__) = clock()), \
		(printf("TIME[%i]: %.4f\n", Z0_MACRO_VAR(__i__), (float)Z0_MACRO_VAR(__end__)/CLOCKS_PER_SEC - (float)Z0_MACRO_VAR(__start__)/CLOCKS_PER_SEC), Z0_MACRO_VAR(__start__) = clock())) \
		for (size_t Z0_MACRO_VAR(__j__) = 0; Z0_MACRO_VAR(__j__) < iter; Z0_MACRO_VAR(__j__) += 1)

#endif
