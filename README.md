# 0xTEST

A testing "library" for `C`.

### API

- Naming your test unit:

```c
void
test_demo_001() {
        Z0_TEST_NAME("DEMO_001");

        //...
}
```

- Describing the test function:

```c
void
test_demo_002() {
       Z0_TEST_NAME("DEMO_002");
       Z0_TEST_DESC("testing 2 == 1.");

       //...
}
```

- Asserting the output:

```c
void
test_demo_003() {
        Z0_TEST_NAME("DEMO_003");
        Z0_TEST_DESC("testing 2 == 3.");

        //...

        Z0_TEST_ASSERT(2 == 3);
}
```

- Benchmarking a part of code. The `Z0_TEST_BENCHMARK` macro, takes 2 arguments. The first parameter is the **count** which tells how many times you want to execute (and print the result) the same benchmark. The second parameter is the **iterations**, which tells how many times you want to execute your part of code:

```c
void
test_benchmark_001() {
        Z0_TEST_NAME("BENCHMARK_001");

        //1st param: {count}
        //2nd param: {iterations}
	Z0_TEST_BENCHMARK(3, 1000) {
		//...
	}
}
```

### Example

Putting everything together:

```c
int
main() {
#ifdef Z0_TEST
        test_001();
        test_002();
        //...
        test_00n();

        benchmark_001();
        benchmark_002();
        //...
        benchmark_00n();
#else
        //...   
#endif
 
        return 0;
}
```
